* * *
"""본 저장소는 과학기술통신부의 재원으로 정보통신기획평가원(IITP)의 지원을 받아 (주) 드림에이스, 경북대학교, 대구경북과학기술원이 함께 수행한 "차량 ECU 응용소프트웨어 개발 및 검증자동화를 위한 가상 ECU기반 차량레벨 통합 시뮬레이션 기술개발(과제고유번호: 2710008421)" 과제의 일환으로 공개된 오픈소스 코드입니다. (2022.04~2024.12)"""

"""본 저장소는 Windows 환경에서 WSL2를 활용하여 Autoas를 실행하기 위한 것으로 가상 ECU의 개발 및 실행 환경을 구성하기 위해 활용됩니다."""
* * *

# 윈도우즈 WSL2 을 활용한 Autoas 실행

# WSL2 Ubuntu-22.04에서 Autoas 실행

![1](/uploads/2e381e7b20cb1b039cd1105f41931d99/1.mp4)  

# WSL2 설치

## 시스템 요구사항

```
1. For x64 systems  
Version 1903 or later, with Build 18362 or later.  
2. For ARM64 systems:   
Version 2004 or later, with Build 19041 or later.  
3. Windows 10 Home, Windows 11 Home 버전도 사용가능  
4. Hyper-V 가상화(Hyper-V Virtualization support)를 지원하는 컴퓨터  
```
  
Windows +  R을 누른 후 msinfo32.exe 입력 > 엔터  

![2](/uploads/3e553c8ed129518622caaa939ee050a7/2.png)  
  
Windows +  R을 누른 후 winver.exe 입력 > 엔터  

![3](/uploads/2c9cca0a0b89484b4f649b0b4ba39469/3.png)  

Ctrl + Shift + ESC 누른 후 CPU 정보에서 가상화: 사용 확인  

![4](/uploads/c8d53ee8bc2b82dbcbf0a81d9863322f/4.png)  

**반드시 Windows 업데이트를 모두 마친 후 진행 필요**  

## 사전 준비

Windows 터미널 설치  
   
[https://apps.microsoft.com/store/detail/windows-terminal/9N0DX20HK701?hl=en-us&gl=us](https://apps.microsoft.com/store/detail/windows-terminal/9N0DX20HK701?hl=en-us&gl=us)  

ubuntu 18.04  

[https://apps.microsoft.com/store/detail/ubuntu-18045-lts/9PNKSF5ZN4SW?hl=ko-kr&gl=kr](https://apps.microsoft.com/store/detail/ubuntu-18045-lts/9PNKSF5ZN4SW?hl=ko-kr&gl=kr)  

ubuntu 20.04  
 
[https://apps.microsoft.com/store/detail/ubuntu-20045-lts/9MTTCL66CPXJ?hl=ko-kr&gl=kr](https://apps.microsoft.com/store/detail/ubuntu-20045-lts/9MTTCL66CPXJ?hl=ko-kr&gl=kr)  

ubuntu 22.04  

[https://apps.microsoft.com/store/detail/ubuntu-22041-lts/9PN20MSR04DW?hl=ko-kr&gl=kr](https://apps.microsoft.com/store/detail/ubuntu-22041-lts/9PN20MSR04DW?hl=ko-kr&gl=kr)  

## Windows 10

```
# Enable the Windows Subsystem for Linux
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart

# Enable Virtual Machine feature
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart

# Set WSL 2 as your default version
wsl --set-default-version 2

```

## Windows 11

관리자 권한으로 Windows Terminal, PowerShell, 명령 프롬프트를 실행한 다음 아래 명령줄을 실행하면 "Linux용 Windows 하위 시스템" 및 "가상 머신 플랫폼" 설치, WSL 커널, GUI 지원 앱 그리고, Linux(기본값 : Ubuntu) 설치까지 한 번에 끝이 납니다. 그리고, "wsl --install" 명령줄로 설치하게 되면 별도의 설정 없이 Linux는 WSL 2 버전으로 설치됩니다.  

```
wsl --install
```

![5](/uploads/2c1a050b2afa08e7f30781a5896eaf4c/5.png)  

## WSL 기본 명령어
```
# 특정 Linux 배포판 설치
wsl --install --distribution <Distribution Name> # wsl --install -d Ubuntu-20.04

# 사용 가능한 Linux 배포판 나열
wsl --list --online

# 설치된 Linux 배포판 나열
wsl --list --verbose

# WSL 버전을 1에서 2로 설정
wsl --set-version <distribution name> <versionNumber>

# 기본 WSL 버전 설정
wsl --set-default-version <Version>

# 기본 Linux 배포판 설정
wsl --set-default <Distribution Name>

# 디렉터리를 홈으로 변경
wsl ~

# PowerShell 또는 CMD에서 특정 Linux 배포판 실행
wsl --distribution <Distribution Name> --user <User Name>
# 그냥 wsl 시 --set-default를 통해 설정한 Linux 실행

# WSL 업데이트
wsl --update

# WSL 상태 확인
wsl --status

# Help 명령
wsl --help

# 특정 사용자로 실행
wsl -u <Username>`, `wsl --user <Username>

# 배포의 기본 사용자 변경
PowerShell

# Shutdown
wsl --shutdown

# 종료
wsl --terminate <Distribution Name>

# 배포판을 TAR 파일로 내보내기
wsl --export <Distribution Name> <FileName>

# 새 배포판 가져오기
wsl --import <Distribution Name> <InstallLocation> <FileName>

# Linux 배포판 등록 취소 또는 제거
wsl --unregister <DistributionName>

# 디스크 또는 디바이스 탑재
wsl --mount <DiskPath>

# 그 외 기타 등등등
```

## 추천 앱

### **Gedit 설치**

Gedit은 GNOME 데스크톱 환경의 기본 텍스트 편집기입니다.  

```
sudo apt install gedit -y
```

편집기에서 bashrc 파일을 시작하려면 다음을 입력합니다.  
`gedit ~/.bashrc`  

### **GIMP 설치**

GIMP는 이미지 조작 및 이미지 편집, 자유 형식 그리기, 다양한 이미지 파일 형식 간의 코드 변환 및 보다 특수한 작업에 사용되는 무료 오픈 소스 래스터 그래픽 편집기입니다.  

```
sudo apt install gimp -y
```

시작하려면 다음을 입력합니다.   
`gimp`  

### **노틸러스 설치**

GNOME 파일이라고도 하는 노틸러스는 GNOME 데스크톱의 파일 관리자입니다. (Windows 파일 탐색기 유사)  

```
sudo apt install nautilus -y
```
시작하려면 다음을 입력합니다.   
`nautilus`  

### **VLC 설치**

VLC는 대부분의 멀티미디어 파일을 재생하는 플랫폼 간 멀티미디어 플레이어 및 프레임워크를 오픈 소스 무료입니다.  

```
sudo apt install vlc -y
```

시작하려면 다음을 입력합니다.  
`vlc`  

### **X11 앱 설치**

X11은 Linux 창 시스템이며 xclock, xcalc 계산기, 잘라내기 및 붙여넣기용 xclipboard, 이벤트 테스트용 xev 등과 함께 제공되는 앱 및 도구의 기타 컬렉션입니다. 자세한 내용은 [x.org 문서를](https://www.x.org/wiki/UserDocumentation/GettingStarted/) 참조하세요.  



```
sudo apt install x11-apps -y
```

시작하려면 사용하려는 도구의 이름을 입력합니다. 다음은 그 예입니다.  

`xcalc`, `xclock`, `xeyes`  

### **Linux용 Google Chrome 설치**

Linux용 Google Chrome을 설치하려면 다음을 수행합니다.  

1. 디렉터리를 임시 폴더로 변경합니다. `cd /tmp`  
2. wget을 사용하여 다운로드합니다. `sudo wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb`  
3. 현재 안정적인 버전을 가져옵니다. `sudo dpkg -i google-chrome-stable_current_amd64.deb`  
4. 패키지를 수정합니다. `sudo apt install --fix-broken -y`  
5. 패키지를 구성합니다. `sudo dpkg -i google-chrome-stable_current_amd64.deb`  

시작하려면 다음을 입력합니다. `google-chrome`  

### **Linux용 Microsoft Teams 설치**

Linux용 Microsoft Teams를 설치하려면 다음을 수행합니다.  

1. 디렉터리를 임시 폴더로 변경합니다. `cd /tmp`  
2. curl을 사용하여 패키지를 다운로드합니다.   
`sudo curl -L -o "./teams.deb" "https://teams.microsoft.com/downloads/desktopurl?env=production&plat=linux&arch=x64&download=true&linuxArchiveType=deb"`  
3. apt를 사용하여 설치합니다. `sudo apt install ./teams.deb -y`  

시작하려면 다음을 입력합니다.  
`teams`  

### **Linux용 Microsoft Edge 브라우저 설치**

[Edge 참가자 사이트의 명령줄을 사용하여 Linux용 Microsoft Edge 브라우저를 설치하는 방법에 대한](https://www.microsoftedgeinsider.com/download/?platform=linux-deb) 정보를 찾습니다. 페이지의 명령줄 설치 섹션에서 **지침 가져오기** 를 선택합니다.  

시작하려면 다음을 입력합니다.  
`microsoft-edge`  

# VCAN 설정

PowerShell에서 설치된 Ubuntu 리스트 및 상태 확인  

```
 wsl --list -v
```
## 가상 인터페이스 지원을 위한 커널 이미지 생성

### 의존성 패키지 설치
```
sudo apt-get update -y

# 빌드시 필요한 종속성
sudo apt-get install make 
sudo apt-get install -y autoconf bison build-essential flex libelf-dev libncurses-dev libssl-dev libtool libudev-dev

# sudo apt-get install ncurses-dev # libncurses-dev로 대체

# /bin/sh: 1: bc: not found 에러 생성 시 
sudo apt-get install -y bc

# make 시 생성되는 에러 BTF: .tmp_vmlinux.btf: pahole (pahole) is not available 에 대응 
sudo apt install -y dwarves 
```
### 커널 빌드
```
# 커널 버전 확인
uname -r
  5.xx.xx.x-microsoft-standard-WSL2 
  # ex) 5.10.102.1-microsoft-standard-WSL2

# 링크에 자기 커널 버전 입력하고 파일 다운로드
wget https://github.com/microsoft/WSL2-Linux-Kernel/archive/refs/tags/linux-msft-wsl-5.xx.xx.x.tar.gz 
# ex) wget https://github.com/microsoft/WSL2-Linux-Kernel/archive/refs/tags/linux-msft-wsl-5.10.102.1.tar.gz
# 만약 해당하는 버전 다운로드가 안되는 경우 WSL2 저장소에서 찾아볼 것
# WSL2 저장소: https://github.com/microsoft/WSL2-Linux-Kernel/releases

# 압축풀기
tar -xf linux-msft-wsl-5.xx.xx.x.tar.gz 
# ex) tar -xf linux-msft-wsl-5.10.102.1.tar.gz

# 설정 및 빌드하기
# (※ make 시 에러 발생할 경우 단계 별로 make clean 후 재시도 할 것)

cd WSL2-Linux-Kernel-linux-msft-wsl-5.xx.xx.x/ 

cat /proc/config.gz | gunzip > .config
make prepare modules_prepare -j $(expr $(nproc) - 1)

# config-wsl 수정, 하단첨부.config 복사 후 맨 하단에 붙여넣기

# config-wsl을 토대로 x86, x64용 kernel 이미지 생성
make -j $(expr $(nproc) - 1) KCONFIG_CONFIG=Microsoft/config-wsl
make modules -j $(expr $(nproc) - 1)
make modules_install
```
### 빌드된 이미지의 적용  
kernal 이미지 복사하여 개별폴더로 이동  
username은 Windows에 로그인 한 이름   
```
cp arch/x86/boot/bzImage /mnt/c/Users/username/
```
# wslconfig 파일 생성

.wslconfig 파일을 username 디렉토리 안에 생성
```
vi /mnt/c/Users/username/.wslconfig
```
.wslconfig의 내용용
```
[wsl2]
kernel=C:\\Users\\username\\bzImage
```
WSL 재부팅
CMD 혹은 POWERSHELL에서 다음 수행행   
```
rem reboot wsl entirely
wsl --shutdown
rem restart wsl
wsl
```

## CAN 통신 설정

WSL 실행   
가상 CAN 인터페이스 생성   
```
sudo modprobe can
sudo modprobe can_raw

sudo modprobe vcan    # 실패할 경우 dmesg 확인 필요

sudo ip link add dev can0 type vcan
sudo ip link set up can0

sudo ip link set can0 mtu 72
```

vCAN통해 데이터 입력, 출력
```
# can-utils 설치하여 vcan test
sudo apt install can-utils

# can0로 데이터 전송
cansend can0 canid#message

# can0로 아무 신호 생성하여 전송
cangen can0

# can0에서 생성되는 신호 확인
candump [option] can0
## candump can0
```
vCAN 시뮬레이션
```
## candump -l can0    # can0 통해 오가는 데이터 기록
### L log2asc -I filename.log can0 # 로그데이터 포맷변경

# filename.log를 토대로 데이터 송수신 재현
canplayer -I filename.log

# 다른 CAN 인터페이스를 통해 송수신된 데이터를 활용한 재현
# can1의 로그 활용 can0에서 재현
canplayer can0=can1 -v -I candump-2015-03-20_123001.log


# can0 통해 송/수신된 제일 마지막 확인인 메세지
cansniffer can0
```
테스트 끝난 후 can0 삭제  

```
# can0 인터페이스 확인 방법
ifconfig
# 또는
ip addr

#can0 삭제
sudo ip link del dev can0
```

### [참조] 커널 Configuration

Config 파일 하단에 붙여넣기 
```
# config-wsl
CONFIG_CC_CAN_LINK=y
CONFIG_CC_CAN_LINK_STATIC=y

# CONFIG_HAMRADIO is not set
CONFIG_CAN=m
CONFIG_CAN_RAW=m
CONFIG_CAN_BCM=m
CONFIG_CAN_GW=m
CONFIG_CAN_J1939=m
CONFIG_CAN_ISOTP=m

# CAN Device Drivers
CONFIG_CAN_VCAN=m
CONFIG_CAN_VXCAN=m
CONFIG_CAN_SLCAN=m
CONFIG_CAN_DEV=m
CONFIG_CAN_CALC_BITTIMING=y
CONFIG_CAN_KVASER_PCIEFD=m
CONFIG_CAN_C_CAN=m
# CONFIG_CAN_C_CAN_PLATFORM is not set
	# CONFIG_CAN_C_CAN_PCI is not set
CONFIG_CAN_CC770=m
# CONFIG_CAN_CC770_ISA is not set
# CONFIG_CAN_CC770_PLATFORM is not set
CONFIG_CAN_IFI_CANFD=m
CONFIG_CAN_M_CAN=m
# CONFIG_CAN_M_CAN_PCI is not set
# CONFIG_CAN_M_CAN_PLATFORM is not set
CONFIG_CAN_PEAK_PCIEFD=m
CONFIG_CAN_SJA1000=m
# CONFIG_CAN_EMS_PCI is not set
# CONFIG_CAN_EMS_PCMCIA is not set
# CONFIG_CAN_F81601 is not set
# CONFIG_CAN_KVASER_PCI is not set
# CONFIG_CAN_PEAK_PCI is not set
# CONFIG_CAN_PEAK_PCMCIA is not set
# CONFIG_CAN_PLX_PCI is not set
# CONFIG_CAN_SJA1000_ISA is not set
# CONFIG_CAN_SJA1000_PLATFORM is not set
CONFIG_CAN_SOFTING=m
CONFIG_CAN_SOFTING_CS=m

# CAN USB interfaces
CONFIG_CAN_8DEV_USB=m
CONFIG_CAN_EMS_USB=m
CONFIG_CAN_ESD_USB2=m
CONFIG_CAN_ETAS_ES58X=m
CONFIG_CAN_GS_USB=m
CONFIG_CAN_KVASER_USB=m
CONFIG_CAN_MCBA_USB=m
CONFIG_CAN_PEAK_USB=m
CONFIG_CAN_UCAN=m
# end of CAN USB interfaces

CONFIG_CAN_DEBUG_DEVICES=y
# end of CAN Device Drivers
```

## 리눅스 배포판 삭제 및 그 외
```
wslconfig.exe [Argument] [Options] # wslconfig.exe /u <distribution name> <versionNumber> 설치된 리눅스-버전 삭제
```
## WSL 완전삭제 (Windows 11 기준)
1) 리눅스 배포판 제거  
  - 설정->앱->앱 및 기능 또는 설치된 앱에서 `ubuntu` 검색하여 설치되어 있는 배포판 모두 제거    
2) WSL 관련 설치 파일 제거  
  - 설정->앱->앱 및 기능 또는 설치된   앱에서 `linux` 검색하여 설치된 기능 제거
      - Windows Subsystem for Linux   Update  
      - Windows Subsystem for Linux WSLg Preview  
3) WSL 기능 해제  
  - 설정->앱->선택적 기능->기타 Windows 기능  
      - `Linux용 Windows 하위 시스템` 체크 박스 해제  
      - `가상머신 플랫폼 체크 박스` 해제  
4) 재부팅  




